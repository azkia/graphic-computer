

function setup() {

    createCanvas(1800, 1000);

    window.StudentNumber = prompt("Please enter your Student number?");
    // window.StudentNumber = "97105085";

    if (StudentNumber == null || StudentNumber.trim() == '' || isNaN(parseInt(StudentNumber))) {
        alert('Please Enter the correct Number')
        throw new Error('Error');
    }

    window.localStudentNumber = StudentNumber.slice(-2);
    window.decimalNumber = localStudentNumber.slice(-2, -1);
    window.lastNumber = localStudentNumber.slice(-1);


    // background(200);

    stroke(127, 63, 120);

    fill(getColorWithDecimalNumber(decimalNumber));

    setShapeWithLastNumber(lastNumber);
}


function draw() {

    // fill(204, 101, 192, 127);

}


function setShapeWithLastNumber(number) {

    switch (parseInt(number)) {
        case 0:
            ellipse(500, 250, 250, 250)
            break;
        case 1:
            ellipse(500, 250, 250, 100)
            break;
        case 2:
            arc(479, 300, 280, 280, PI, TWO_PI);
            break;
        case 3:
            triangle(50, 95, 78, 40, 106, 95);
            break;
        case 4:
            rect(500, 250, 200, 200);
            break;
        case 5:
            polygon(500, 250, 80, 5);
            break;
        case 6:
            polygon(500, 250, 80, 6);
            break;
        case 7:
            rotate(PI / 3.0);
            rect(300, -250, 200, 200);
            break;
        case 8:

            star(500, 250, 30, 70, 5);
            break;
        case 9:
            rect(500, 250, 500, 200);
            break;

        default:
            console.log('We couldn\'t find your shape.')
            break;
    }

}



function polygon(x, y, radius, npoints) {
    let angle = TWO_PI / npoints;
    beginShape();
    for (let a = 0; a < TWO_PI; a += angle) {
        let sx = x + cos(a) * radius;
        let sy = y + sin(a) * radius;
        vertex(sx, sy);
    }
    endShape(CLOSE);
}

function star(x, y, radius1, radius2, npoints) {
    let angle = TWO_PI / npoints;
    let halfAngle = angle / 2.0;
    beginShape();
    for (let a = 0; a < TWO_PI; a += angle) {
        let sx = x + cos(a) * radius2;
        let sy = y + sin(a) * radius2;
        vertex(sx, sy);
        sx = x + cos(a + halfAngle) * radius1;
        sy = y + sin(a + halfAngle) * radius1;
        vertex(sx, sy);
    }
    endShape(CLOSE);
}



function getColorWithDecimalNumber(number) {
    colorsArray = [
        '#ef1548',
        '#41ef15',
        '#efec15',
        '#ef7f15',
        '#efb515',
        '#e115ef',
        '#3d15ef',
        '#a29fad',
        '#f975c7',
        '#87f975',
    ];

    return colorsArray[number];
}
